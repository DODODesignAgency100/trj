<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package TRJ
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e( 'Skip to content', 'trj' ); ?></a>

	<header id="masthead" class="site-header trj-header">

		<div class="main-nav">
			<div class="site-branding">
				<?php

					the_custom_logo();

				?>
			</div>

			<nav id="site-navigation" class="main-navigation trj-nav">

				<button style="display:none;" class="menu-toggle" aria-controls="primary-menu" aria-expanded="false">
					<?php esc_html_e( 'Primary Menu', 'trj' ); ?>
				</button>
				<div class="hamburger">
					<span></span>
					<span></span>
					<span></span>
				</div>
				
				

					<div class="menu-center">
						<?php
						wp_nav_menu(
							array(
								'theme_location' => 'menu-1',
								'menu_id'        => 'primary-menu',
							)
						);
						?>
					</div>

					<div class="menu-right">
						<ul>
							<a href="http://trj.dodo.ng/contact" class="trj-cta">Book Consultation</a>
						</ul>
					</div>

			</nav>

		</div>

		<div class="mobile-menu">

			<div class="menulr">
					<?php
						wp_nav_menu(
							array(
								'theme_location' => 'menu-1',
								'menu_id'        => 'primary-menu',
							)
						);
					?>
					<a href="/contact" class="trj-cta">Book Consultation</a>
			</div>

		</div>

	</header>
